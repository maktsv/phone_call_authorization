import os
import sys
import psycopg2
import logging
import datetime
import requests


def fetch_acd():

    # Connect to CDR DB

    conn_string = "dbname='cdr' host='192.168.0.20' port='5432' user='andspi' password='6v2QYr'"

    try:
        conn = psycopg2.connect(conn_string)
    except psycopg2.OperationalError as e:
        log.error('Unable to Connect! %s' % str(e))
        sys.exit(1)
    else:
        pass

    #CD Date format

    current_day = datetime.date.today()
    pyyyymm = current_day.strftime('%Y%m')
    pyyyymmdd = current_day.strftime('%Y%m%d')


    #A215 ACD Query

    query_calculate_acd = f"""
    SELECT  distinct substr(bin, 1, 3),
      sum(case when billmseconds=0 then 0 else 1 end) as realcalls,
      sum(case when billmseconds=0 then 1 else 0 end) as zerocalls,
      round(sum(billmseconds/60000.0)) as minutes,
      round(sum(billmseconds/60000.00) / sum(case when billmseconds !=0 then 1 end), 2) as acd
    FROM cd{pyyyymm}.cd{pyyyymmdd}
    WHERE intrunk = 'A215'
      AND finished >= NOW() - INTERVAL '2HOURS'
      AND last = 1
    GROUP BY substr(bin, 1, 3)
    HAVING round(sum(billmseconds/60000.00) / sum(case when billmseconds !=0 then 1 end), 2) > 2
    ORDER BY 5 DESC
    """

    cur = conn.cursor()
    cur.execute(query_calculate_acd)
    data = cur.fetchall()
    conn.close()

    return data

def send_alert(cases):

    # Mail Module Import

    sys.path.insert(0, "/home/andspi/Scripts/_packages")
    from main.main import send_mail

    # Alert

    for c in cases:

        message = f"""
            <p>The ACD value is above 2 minutes!</br></br>
            Code: {c[0]}</br>
            Real Calls: {c[1]}</br>
            Zero Calls: {c[2]}</br>
            Minutes: {str(c[3])}</br>
            ACD: {str(c[4])}</br>
        """

        send_mail(
            send_from='no-reply@topconnect.ee',
            send_to=['support@topconnect.ee'],
            send_bcc=['qa@topconnect.ee'],
            subject=f'Phone Call Authorization ACD Alert',
            file=None,
            html=message
        )

        a = c[0]
        print(a)
        b = str(a)
        print(b)
        req = "http://10.31.2.55:8000/set?block_prefix=" + b + ""
        requests.get(req, verify=False)

def check_acd():

    cases = fetch_acd()
    if cases:
        send_alert(cases)
    else:
        logging.info("ACD value is OK.")